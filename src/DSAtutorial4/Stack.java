package DSAtutorial4;

public class Stack {
	
	//Operations needed => isEmpty, peek, push, pop
	
	private int[] myStack;
	private int top;
	private int capacity;
	
	public Stack(int capacity) {
		this.capacity = capacity > 0? capacity:0;
		this.myStack = new int[this.capacity];
		this.top = -1;
	}
	
	public boolean isEmpty() {
		return top == -1;
	}
	
	public int push(int value) {
		if(isFull()) return -1;
		
		this.myStack[top+1] = value;
		this.top++;
		return value;
	}
	
	public int peek() {
		if(isEmpty()) return -1;
		
		return this.myStack[top];
	}
	
	public int pop() {
		if(isEmpty()) return -1;
		
		return this.myStack[top--];
	}
	
	private boolean isFull() {
		return (top + 1) == capacity;
	}

	@Override
	public String toString() {
		String stack = "{ ";
		for (int i = 0; i < capacity; i++) {
			stack += this.myStack[i]+", "; 
		}
		stack+="}";
		
		return stack;
	}
	
	public String toStringValuesOnly() {
		if(isEmpty()) return "{}";
		
		String stack = "{ ";
		for (int i = 0; i <= top; i++) {
			stack += this.myStack[i]+", "; 
		}
		stack+="}";
		
		return stack;
	}
	
	

}
