package DSAtutorial4;

public class StackChar {
	private char[] myStack;
	private int top;
	private int capacity;
	
	public StackChar(int capacity) {
		this.capacity = capacity > 0? capacity:0;
		this.myStack = new char[this.capacity];
		this.top = -1;
	}
	
	public boolean isEmpty() {
		return top == -1;
	}
	
	public void push(char value) {
		if(isFull()) return;
		
		this.myStack[top+1] = value;
		this.top++;
	}
	
	public char peek() {
		if(isEmpty()) return ' ';
		
		return this.myStack[top];
	}
	
	public char pop() {
		if(isEmpty()) return ' ';
		
		return this.myStack[top--];
	}
	
	private boolean isFull() {
		return (top + 1) == capacity;
	}
}
