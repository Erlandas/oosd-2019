package DSAtutorial4;

public class Driver {

	public static void main(String[] args) {
		
		Stack binStack = new Stack(10);

		int decnum = 253;
		String num = decToBin(decnum,binStack);
		System.out.println("Decimal => " +decnum+ " to binary => " +num);
		
		StackChar palStack = new StackChar(10);
		String word = "abba";
		String word1 = "word";
		System.out.println("Word is checked [" +word+ "] is palindrom => " + isPalindrome(word, palStack));
		palStack = new StackChar(10);
		System.out.println("Word is checked [" +word1+ "] is palindrom => " + isPalindrome(word1, palStack));
		
		int[][] twoDA = {
				{2,3,5,7},
				{13,17,19,23},
				{31,37,41,43},
				{53,59,61,67},
		};
		
		//Expected output 13+31+37+53+59+61=254
		System.out.println("Sum of diagonal => " +countDiagonal(twoDA));
		

	}
	
	//Question 4.
	//	--	Decimal to binary conversion
	public static String decToBin(int dec, Stack st) {
		int mod = 0;
		
		while(dec != 0) {
			mod = dec % 2;
			dec = dec/2;
			st.push(mod);
		}
		
		String bin = "";
		while(!st.isEmpty()) {
			bin+= ""+st.pop();
		}
		
		return bin;
		
	}
	
	//Question 5.
	//	--	Palindrome checker
	public static boolean isPalindrome(String word,StackChar st) {
		for (int i = 0; i < word.length(); i++) {
			st.push(word.charAt(i));
		}
		for (int i = 0; i < word.length(); i++) {
			if(st.pop() != word.charAt(i)) {
				return false;
			}
		}
		return true;
	}
	
	//Question 6.
	//	--	Count diagonal of 2D array
	public static int countDiagonal(int[][] a) {
		int sum = 0;
		for (int row = 1; row < a.length; row++) {
			for (int col = 0; col < row; col++) {
				sum += a[row][col];
			}
		}
		return sum;
	}
	

}
