package lab2b;

public class Rectangle {

	//Instance variables
	private int length;
	private int width;

	//Default constructor with default values
	public Rectangle() {
		this.length = 1;
		this.width = 1;
	}

	//Overloaded constructor
	public Rectangle(int length, int width) {
		this.length = ((length > 0) && (length <= 40)) ? length : 1;
		this.width = ((width > 0) && (width <= 40)) ? width : 1;
	}

	//Getters and Setters
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	//Extra methods
	//	1.	Returns area of rectangle
	public int getArea() {
		return this.length * this.width;
	}

	//	2.	Returns perimeter
	public int getPerimeter() {
		return 2 * getArea();
	}

	//	3.	Prints hollow rectangle using set width and height
	public void printRectangle() {

		for (int row = 0; row < this.length; row++) {
			for (int col = 0; col < this.width; col++) {
				if (row == 0 || row == this.length - 1 || col == 0 || col == this.width - 1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

	//	4.	To String method
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Length = " + this.length + ", Width = " + this.width;
	}

}
