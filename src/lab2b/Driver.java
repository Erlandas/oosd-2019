package lab2b;

//Student Name 			: Erlandas Bacauskas
//Student Id Number		: C00242521
//Date 					: 29/9/2019
//Purpose 				: Rectangle lab exercise description in (lab 2b Exercise.txt)

public class Driver {

	public static void main(String[] args) {
		
		//Create instance of rectangle
		Rectangle testRectangle = new Rectangle(5,7);
		
		//Testing instance methods
		System.out.println("Area of rectangle " + testRectangle.getArea());
		System.out.println("Perimeter of rectangle " + testRectangle.getPerimeter());
		testRectangle.printRectangle();

	}

}
