package lab4;

//TODO age criteria 

import java.util.Scanner;

public class Employee {

	//Static instance variables
	private static int empNumber = 1000;
	
	//Instance variables
	private String name;
	private int age;
	private Car car;
	private boolean isManager;
	private Address address;
	private int employeeNumber;
	
	//Default constructor
	public Employee() {
		this("",-1,false,null);
	}
	
	//Overloaded constructor with parameters
	public Employee(String name, int age, boolean isManager, Address address) {
		this.name = name;
		this.age = age;
		this.address = address;
		this.isManager = isManager;
		this.car = getCarDetails(isManager);
		
		this.employeeNumber = this.empNumber++;
		
	}
	
	public static int getEmpNumber() {
		return empNumber;
	}

	//Getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCar() {
		if(this.car != null) return car.toString();
		return "No details of a car recorded";
	}

	public void setCar() {
		
		//ERROR check if not yet set to manager
		if(this.isManager == false) {
			System.err.println("Only managers alowed to have a car");
			return;
		}
		
		this.car = getCarDetails(this.isManager);
	}

	public boolean isManager() {
		return isManager;
	}

	public void setManager(boolean isManager) {
		//ERROR check if change from manager to staff get rid of a car if it was created
		if(this.car != null && !isManager) this.car = null;
		this.isManager = isManager;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	//Utility private methods
	private Car getCarDetails(boolean isManager) {

		if(!isManager) return null;
		
		Scanner in = new Scanner(System.in);
		
		String make;
		String model;
		int year;
		boolean hasNextInt = false;
		
		//Get details of a car
		System.out.println("Enter car make: ");
		make = in.nextLine();
		System.out.println("Enter car model: ");
		model = in.nextLine();
		System.out.println("Enter car year: ");
		year = in.nextInt();
		
		//Create and return instance of a car
		Car nCar = new Car(make, model, year);
		return nCar;
	}

	
	//toString
	@Override
	public String toString() {
		return (this.isManager?"Manager " : "Staff ") + "[name=" + name + ", age=" + age + ", isManager=" + isManager + ", address="
				+ address + ", employeeNumber=" + employeeNumber + "]" + (this.isManager? this.getCar():"");
	}
	
	
}
