package lab4;

import java.util.Arrays;

public class Office {
	
	//Static variables
	private static int officeNumberTracker = 100;
	
	//Instance variables
	private Employee[] empRecords;
	private String departament;
	private int departamentRoomNum;
	private int recordCounterEmp;
	
	//Constructors
	public Office() {
		this("");
	}
	
	public Office(String departament) {
		this.departament = departament;
		this.empRecords = new Employee[2];
		this.departamentRoomNum = this.officeNumberTracker++;
		this.recordCounterEmp = 0;
	}
	
	//Getters and Setters
	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	public Employee[] getEmpRecords() {
		return empRecords;
	}

	public int getDepartamentRoomNum() {
		return departamentRoomNum;
	}
	
	
	
	//Random methods
	public boolean addEmployee(Employee employee) {
		if(this.recordCounterEmp >= 2) return false;
		
		this.empRecords[this.recordCounterEmp++] = employee;
		return true;
	}
	
	public boolean clearAllEmpRecords() {
		for (Employee employee : empRecords) {
			employee = null;
			this.recordCounterEmp--;
		}
		return true;
	}
	
	public int numOfEmpAssigned() {
		return this.recordCounterEmp;
	}
	
	public void listEmployees() {
		for (int i = 0; i < empRecords.length; i++) {
			if(this.empRecords[i] != null) {
				System.out.println(this.empRecords[i].toString());
			}
		}
	}
	
    //ToStirng
	@Override
	public String toString() {
		return "Office [empRecords=" + Arrays.toString(empRecords) + ", departament=" + departament
				+ ", departamentRoomNum=" + departamentRoomNum + ", recordCounterEmp=" + recordCounterEmp + "]";
	}
	
	
	
	

}
