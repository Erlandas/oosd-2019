package lab4;

public class Car {
	
	//Very basic class to store car details
	//Left only what is really needed for current project
	//Instance variable, overloaded constructor and toString
	//Also as utility method
	
	//Instance variables
	private String make;
	private String model;
	private int year;
	
	//Overloaded constructor
	public Car(String make, String model, int year) {
		this.make = make;
		this.model = model;
		this.year = checkYear(year);
	}

	//Utility method
	private int checkYear(int year) {
		
		//Assumption as legal criteria for car year is between and including 1980-2019
		if(year <= 2019 && year >= 1980) return year;
		else return -1;
	}
	
	//To String
	@Override
	public String toString() {
		return "Car [make=" + make + ", model=" + model + ", year=" + year + "]";
	}
	
	
	
	

}
