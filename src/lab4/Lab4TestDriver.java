package lab4;

import java.util.Scanner;

public class Lab4TestDriver {
	
	public static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		
		Office[] offices = new Office[3];
		Employee[] empRecords = new Employee[5];
		initialise(offices);

		
		int option = 0;
		while(option != 5) {
			option = menu();
			switch (option) {
				case 1:
					listOffices(offices);
					break;
				case 2:
					createEmployee(offices, empRecords);
					break;
				case 3:
					for (Office office : offices) {
						if(office != null) {
							office.listEmployees();
						}
					}
				case 4:
					menu();
					break;
				case 5:
					System.out.println("Working Hours are over bye!!!");
					break;
				default:
				break;
			}
		}
		scanner.close();
		
		
	}
	
	private static void createEmployee(Office[] o, Employee[] e) {
		
		if(Employee.getEmpNumber()-1000 > 5) {
			System.out.println("Max number of employee records reached");
			return;
		}
		
		String eName;
		int eAge;
		String managerStaff;
		boolean isManager = false;
		String street;
		String cityTown;
		String county;
		
		System.out.println("Enter name: ");
		eName = scanner.next();
		System.out.println("Enter age : ");
		eAge = scanner.nextInt();
		System.out.println("Type (Manager/Staff) : ");
		managerStaff = scanner.next();
		if(managerStaff.equalsIgnoreCase("MANAGER")) {
			isManager = true;
		}
		
		System.out.println("Address street : ");
		street = scanner.next();
		System.out.println("Address city town : ");
		cityTown = scanner.next();
		System.out.println("Address county : ");
		county = scanner.next();
		
		Address a = new Address(street, cityTown, county);
		Employee emp = new Employee(eName, eAge, isManager, a);
		
		for (Office office : o) {
			if(office.addEmployee(emp)) break;
		}
	}
	
	//List offices
	private static void listOffices(Office[] a) {
		for(int i = 0; i < a.length; i++) {
			System.out.println("#" + (i+1) + " : " + a[i].getDepartament());
		}
	}
	
	//Initialise 3 offices as per question
	private static void initialise(Office[] offices) {
		Office office1 = new Office("IT departament");
		Office office2 = new Office("Auditing");
		Office office3 = new Office("Cathering");
		
		offices[0] = office1;
		offices[1] = office2;
		offices[2] = office3;
	}
	
	//Method for menu
	private static int menu() {
		String menu = 	"\tAvailable Options" +
						"\n\t1.  List all offices" + 
						"\n\t2.  Create a new employee record" +
						"\n\t3.  List all employees" +
						"\n\t4.  Show options" +
						"\n\t5.  Exit";
		
		System.out.println(menu);
		int option = scanner.nextInt();
		return option;
	}

}
