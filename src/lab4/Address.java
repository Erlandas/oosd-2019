package lab4;

public class Address {
	
	//instance variables
	private String street;
	private String cityTown;
	private String county;
	
	//Default constructos
	public Address() {
		this("Not set","Not set","Not set");
	}
	
	//Overloaded constructor
	public Address(String street, String cityTown, String county) {
		this.street = street;
		this.cityTown = cityTown;
		this.county = county;
	}

	//Getters and setters
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCityTown() {
		return cityTown;
	}

	public void setCityTown(String cityTown) {
		this.cityTown = cityTown;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	//ToString
	@Override
	public String toString() {
		return "Address [street=" + street + ", cityTown=" + cityTown + ", county=" + county + "]";
	}
	
	
	
	

}
