package lab7;

public class AnimalTest {

	public static void main(String[] args) {
		Animal cat = new Cat("Gucci", 6, 'M');
		Animal dog = new Dog("Jerry", 5, 'M');
		Animal cow = new Cow("Daisy", 2, 'f');
		
		Animal[] animals = new Animal[3];
		animals[0] = cat;
		animals[1] = dog;
		animals[2] = cow;
		
		for (Animal animal : animals) {
			System.out.println(animal.toString());
			animal.eat();
			animal.sleap();
			animal.makeSound();
			divider();
		}
		
		Vet vet = new Vet("Mister Butcher the Third");
		
//		vet.vaccinate(cat);
//		divider();
//		vet.vaccinate(dog);
//		divider();
//		vet.vaccinate();
//		divider();
//		vet.vaccinate(cow);
		
		vet.vaccinateMe(animals);

	}
	
	
	private static void divider() {
		System.out.println("-------------------------\n");
	}

}
