package lab7;

public class Dog extends Animal {
	
	public Dog(String name, int age, char gender) {
		super(name, age, gender);
	}

	@Override
	public void eat() {
		System.out.println("Dog eats");
	}

	@Override
	public void sleap() {
		System.out.println("Dog sleeps");
	}

	@Override
	public void makeSound() {
		System.out.println("Bark Bark Bark");
	}
	
	@Override
	public void vaccinate(String vetName) {
		System.out.println("Dog has been vaccinated by " +vetName);
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
}
