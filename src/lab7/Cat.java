package lab7;

public class Cat extends Animal {
	
	public Cat(String name, int age, char gender) {
		super(name, age, gender);
	}

	@Override
	public void eat() {
		System.out.println("Cat eats");
	}

	@Override
	public void sleap() {
		System.out.println("Cat sleeps");
	}

	@Override
	public void makeSound() {
		System.out.println("Meow Meow Meow");
	}
	
	@Override
	public void vaccinate(String vetName) {
		System.out.println("Cat has been vaccinated by " +vetName);
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	

}
