package lab7;

public class Cow extends Animal{

	public Cow(String name, int age, char gender) {
		super(name, age, gender);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void eat() {
		System.out.println("Cow is eating");
	}

	@Override
	public void sleap() {
		System.out.println("Cow is sleaping");
	}

	@Override
	public void makeSound() {
		System.out.println("Moo");
	}

	@Override
	public void vaccinate(String vetName) {
		System.out.println("Cow has been vaccinated by " +vetName);
		System.out.println(toString());
	}
	
}
