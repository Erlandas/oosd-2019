package lab7;

public abstract class Animal {
	//Instance variables
	private String name;
	private int age;
	private char gender;
	
	//Constructor
	public Animal(String name, int age, char gender) {
		this.name = name;
		this.age = age;
		this.gender = checkGender(gender);
	}
	
	//Getters and Setters
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = checkGender(gender);
	}
	
	//Methods

	public abstract void eat();
	public abstract void sleap();
	public abstract void makeSound();
	
	private char checkGender(char gender) {
		return (gender == 'm' || gender == 'f') || (gender == 'M' || gender == 'F')? gender:'x';
	}
	
	public void vaccinate(String vetName) {
		System.out.println(vetName + " is vaccinating animal");
	}

	@Override
	public String toString() {
		return "Name: "+name+", Age: "+age+", Gender: " +gender;
	};
	
	
}
 