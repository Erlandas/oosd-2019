package lab7;

public class Vet {
	
	private String name;

	public Vet(String name) {
		this.name = name;
	};
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void vaccinate(Animal animal) {
		animal.vaccinate(name);
	}
	
	//Jasons example
	public void vaccinateMe(Animal[] animals) {
		for (Animal animal : animals) {
			System.out.println(this.name + " is vaccinating");
			System.out.println(animal.getClass().getName());
			System.out.println("the " + animal.getClass().getName() + " has been vaccinated " + animal.toString());
			System.out.println("-------------------------\n");
		}
	}
	
	public void vaccinate() {
		System.out.println(name + " is vaccinating");
	}
	
	

}
