package lab6;

import java.util.Arrays;

public class Student extends Person {
	
	//Instance variables
	private int numCourses;
	private String[] courses;
	private int[] grades;
	private int courseCount;

	//Constructor
	public Student(String name, String address, int numCourses) {
		super(name, address);
		this.numCourses = numCourses;
		this.courses = new String[this.numCourses];
		this.grades = new int[this.numCourses];
		this.courseCount = 0;
	}
	
	//Methods
	public void addCourseGrade(String course, int grade) {
		if(this.courseCount>=this.numCourses) return;
		this.courses[this.courseCount] = course;
		this.grades[this.courseCount] = grade;
		this.courseCount++;
	}
	
	public void printGrades() {
		if(this.courseCount == 0) return;
		System.out.println("--<<COURSE AND GRADES>>--");
		for (int i = 0; i < this.courseCount; i++) {
			System.out.println("#" +(i+1)+ "Course : " + this.courses[i] + ", Grade : " + this.grades[i]);
		}
	}
	
	public double getAverageGrade() {
		
		if(this.courseCount == 0) return -1.0d;
		else if(this.courseCount == 1) return (double)this.grades[this.courseCount-1];
		
		double avg = 0.0d;
		for (int i = 0; i < this.courseCount; i++) {
			avg+=(double)this.grades[i];
		}
		return (double)(avg/this.courseCount);
	}

	@Override
	public String toString() {
		return super.toString() + "Student [numCourses=" + numCourses + ", courses=" + Arrays.toString(courses) + ", grades="
				+ Arrays.toString(grades) + ", courseCount=" + courseCount + "]";
	}
	
	
}
