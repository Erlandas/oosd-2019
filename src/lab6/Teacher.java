package lab6;

import java.util.Arrays;

public class Teacher extends Person {

	//Instance variables
	private int numCourses;
	private String[] courses;
	private int courseCount;
	
	//Constructor
	public Teacher(String name, String address, int numCourses) {
		super(name, address);
		this.numCourses = numCourses;
		this.courses = new String[this.numCourses];
		this.courseCount = 0;
	}
	
	//Add course method
	public boolean addCourse(String course) {
		if(this.courseCount >= this.numCourses) return false;
		
		//Check for duplicates
		for (int i = 0; i < courseCount; i++) {
			if(courses[i].equals(course)) {
				return false;
			}
		}
		
		this.courses[this.courseCount] = course;
		this.courseCount++;
		return true;
	}
	
	//Remove course method
	public boolean removeCourse(String course) {
		//If there is no courses that means it cannot be removed as there is nothing to remove return false
		if(this.courseCount <= 0) return false;
		
		//Iterate through array of courses
		for (int i = 0; i < this.courseCount; i++) {
			//If course found remove it
			if (course.equalsIgnoreCase(this.courses[i])) {
				if(i<=this.courseCount) {
					for (int j = i; j < this.courseCount-1; j++) {
						this.courses[j] = this.courses[j+1];
					}
					this.courses[--this.courseCount] = null;
				}
				return true;
			}
		}
		
		return false;
	}
	
	public void printCourses() {
		System.out.println("--<<COURSES>>--");
		for (int i = 0; i < this.courseCount; i++) {
			System.out.println("#" +(i+1)+ "Course : " + this.courses[i]);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "Teacher [numCourses=" + numCourses + ", courses=" + Arrays.toString(courses) + ", courseCount="
				+ courseCount + "]";
	}
	
	
	
	
 }
