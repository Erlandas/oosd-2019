package garbageCollection;

public class EmployeeTest {

	public static void main(String[] args) {

		System.out.println("Employees before instantiation : " + Employee.getCount());

		Employee e1 = new Employee("Susan", "Baker");
		Employee e2 = new Employee("Bob", "Jones");
		System.out.println("Employees after instantiation : " + e1.getCount());
		System.out.println("Employee 1: " + e1.getFName() + " " + e1.getLName());
		System.out.println("Employee 2: " + e2.getFName() + " " + e2.getLName());
		e1 = null;
		e2 = null;

		System.gc(); // explicit call to garbage collector
		
		while(Employee.getCount() != 0) {
			System.out.println("Waiting for garbage routine to finish!!!");
		}
		
		System.out.println("Employees after garbage collection : " + Employee.getCount());
	}

}
