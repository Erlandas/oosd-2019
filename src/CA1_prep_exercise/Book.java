package CA1_prep_exercise;

public class Book {

	//Static field storing book ISBN at higher level
	private static int ISBNtracker = 1;

	//Instance variables/attributes
	private String bookName;
	private String author;
	private String genre;
	private char status;
	private int ISBN;

	//Default constructor
	public Book() {
		// this("", "", "");

		setBookName("");
		setAuthor("");
		setGenre("");
		this.status = 'A';
		this.ISBN = ISBNtracker;
		this.ISBNtracker++;
	}

	//Overloaded constructor
	public Book(String bookName, String author, String genre) {
		this.bookName = bookName;
		this.author = author;
		this.genre = genre;
		this.status = 'A';
		this.ISBN = ISBNtracker;
		this.ISBNtracker++;
	}

	//Getters and setters
	public String getBookName() {
		return this.bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return this.genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getISBN() {
		return this.ISBN;
	}

	//Extra methods
	public void borrow() {
		if (this.status == 'A') {
			this.status = 'B';
			System.out.println("You borrowed a book " + this.bookName + "\nBook status changed to " + this.status);
			return;
		}
		System.out.println("Sorry book its not available at the moment " + "\nCurrent status is " + this.status);
	}

	public void returnBook() {
		if (this.status == 'B') {
			this.status = 'A';
			System.out
					.println("Thank You for returning a book " + this.bookName + "\nBook status set to " + this.status);
			return;
		}
		System.out.println("Sorry you cant return returned book");
	}

	//Returns amount of books if ISBN starts from 1 that means ISBNtracker - 1 = current amount of books
	public static int totalAmount() {
		return ISBNtracker - 1;
	}

	//TO String
	@Override
	public String toString() {
		return "Book Name : " + this.bookName + "\nAuthor : " + this.author + "\nGenre : " + this.genre
				+ "\nAvailability Status : " + this.status + "\nISBN : " + this.ISBN + "\n\n";
	}
}
