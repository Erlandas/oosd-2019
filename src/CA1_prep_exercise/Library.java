package CA1_prep_exercise;

import java.util.Scanner;

public class Library {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		//Few variables for testing purposes
		String bookName;
		String author;
		String genre;

		//Create array to hold books size 3
		Book[] books = new Book[3];

		//Iterate through array and store details for a books
		for (int i = 0; i < books.length; i++) {

			System.out.println("Book Details :");
			System.out.print("Enter book name : ");
			bookName = input.nextLine();
			System.out.print("Enter author : ");
			author = input.nextLine();
			System.out.print("Enter genre : ");
			genre = input.nextLine();
			System.out.println("-----------------------");

			//Instantiate book type objects with supplied data
			books[i] = new Book(bookName, author, genre);
		}
		input.close();

		//Testing methods from Book() class
		books[1].borrow();
		System.out.println("-----------------------");
		books[1].borrow();
		System.out.println("-----------------------");
		books[1].returnBook();
		System.out.println("-----------------------");
		books[1].returnBook();
		System.out.println("-----------------------");

		for (Book book : books) {
			System.out.println(book.toString());
		}
		System.out.println("-----------------------");
		System.out.println("Total amount of books : " + Book.totalAmount());
	}

}
