package lab3.Q2_Q3;

//Student Name 			: Erlandas Bacauskas
//Student Id Number		: C00242521
//Date 					: 4/10/2019
//Purpose 				: Savings account lab exercise description in (lab 3 Exercises.txt)

public class SADriver {

	public static void main(String[] args) {
		
		SavingsAccount saver1 = new SavingsAccount(2000.00d);
		SavingsAccount saver2 = new SavingsAccount(3000.00d);
		
		SavingsAccount.modifyInterestRate(0.04f);
		
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		
		System.out.println(saver1);
		System.out.println(saver2);
		
		SavingsAccount.modifyInterestRate(0.05f);
		
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		
		System.out.println(saver1);
		System.out.println(saver2);
		

	}

}
