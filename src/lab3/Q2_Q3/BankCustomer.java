package lab3.Q2_Q3;

public class BankCustomer {
	
	//Instance variables
	private String name;
	private Address address;
	
	//--	basic aggregation example
	//--	Savings account can exists before a customer
	//--	Also as address can exists before and customer created
	
	private SavingsAccount[] savingsAccounts;
	private int accCounter;

	//Default constructor
	public BankCustomer() {
		this("",new Address());
	}
	
	//Overloaded constructor
	public BankCustomer(String name, Address address) {
		this.name = name;
		this.address = address;
		this.accCounter = 0;
		savingsAccounts = new SavingsAccount[3];
	}

	//Getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address.toString();
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public SavingsAccount[] getSavingsAccounts() {
		return savingsAccounts;
	}
	
	//Extra methods

	//	--	Using boolean as return type for validation
	public boolean addAccount(SavingsAccount sa) {
		if(this.accCounter < 3) {
			savingsAccounts[accCounter] = sa;
			accCounter++;
			System.out.println("Account added");
			return true;
		}
		
		System.out.println("Sorry, you reached max limit of savings accounts allowed");
		return false;
	}
	
	public double balance(SavingsAccount[] savingsAccounts) {
		//If no accounts added don't waste time and resources RETURN
		if(accCounter == 0) return 0.00d;
		
		double totalBalance = 0;
		
		for (SavingsAccount sa : savingsAccounts) {
			if(sa != null) {
				totalBalance += sa.getSavingsBalance();
			}
		}
		
		return totalBalance;
		
	}

	@Override
	public String toString() {
		return "Account name : " + getName() + "\n" +
				"Address : \n" + getAddress();
	}
	
	
	
	
	
	
	
	
	
	
}
