package lab3.Q2_Q3;

//Student Name 			: Erlandas Bacauskas
//Student Id Number		: C00242521
//Date 					: 4/10/2019
//Purpose 				: Bank Customer lab exercise description in (lab 3 Exercises.txt)

public class BCDriver {

	public static void main(String[] args) {
		
		//Create instance of Address class
		Address freddysAddress = new Address("13 Elm Street","Carlow","Co. Carlow","Ireland");
		//System.out.println(freddysAddress);
		
		//Create instance of bankCustomer class
		BankCustomer freddyBankCustomer = new BankCustomer("fred", freddysAddress);
		SavingsAccount.modifyInterestRate(0.06f);
		//Create instances of savings account
		//--	created one extra for testing if counter works as intended
		SavingsAccount sa1 = new SavingsAccount(1000.00);
		SavingsAccount sa2 = new SavingsAccount(2000.00);
		SavingsAccount sa3 = new SavingsAccount(3000.00);
		SavingsAccount sa4 = new SavingsAccount(4000.00);
		
		sa1.calculateMonthlyInterest();
		sa2.calculateMonthlyInterest();
		sa3.calculateMonthlyInterest();
		
		freddyBankCustomer.addAccount(sa1);
		freddyBankCustomer.addAccount(sa2);
		freddyBankCustomer.addAccount(sa3);
		freddyBankCustomer.addAccount(sa4);
		
		
		
		System.out.println(freddyBankCustomer.balance(freddyBankCustomer.getSavingsAccounts()));
		
		for (SavingsAccount sa : freddyBankCustomer.getSavingsAccounts()) {
			if(sa != null) {
				System.out.println(sa.toString());
			}
		}
		
		System.out.println(freddyBankCustomer.toString());
		

	}

}
