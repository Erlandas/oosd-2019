package lab3.Q2_Q3;

public class SavingsAccount {
	
	//Instance variables
	//	--	Static
	private static int accNoTracker = 0;
	private static float annualInterestRate;
	
	//	--	Non-static
	private int accNo;
	private double savingsBalance;
	
	//Default constructor
	public SavingsAccount() {
		this.accNo = ++this.accNoTracker;
		this.savingsBalance = -1;
	}

	//overloaded constructor
	public SavingsAccount(double savingsBalance) {	
		this.savingsBalance = savingsBalance;
		this.accNo = ++this.accNoTracker;
	}

	//Getters and Setters
	public static float getAnnualInterestRate() {
		return annualInterestRate;
	}

	public int getAccNo() {
		return accNo;
	}

	public double getSavingsBalance() {
		return savingsBalance;
	}

	public void setSavingsBalance(double savingsBalance) {
		this.savingsBalance = savingsBalance;
	}
	
	//Extra methods
	public static void modifyInterestRate(float annualInterestRate) {
		SavingsAccount.annualInterestRate = annualInterestRate;
	}
	
	public void calculateMonthlyInterest() {
		double interest = (SavingsAccount.annualInterestRate*getSavingsBalance())/12;
		setSavingsBalance(getSavingsBalance()+interest);
	}

	//ToString
	@Override
	public String toString() {
		return "Savings Account : AccNo => " +getAccNo() + ", Balance => " +getSavingsBalance();
	}
	
	
	
	
	

}
