package lab3.Q2_Q3;

public class Address {
	
	//Instance variables
	private String streetLine;
	private String town;
	private String county;
	private String country;
	
	//Default constructor
	public Address() {
		this("","","","");
	}

	//Overloaded constructor
	public Address(String streetLine, String town, String county, String country) {
		this.streetLine = streetLine;
		this.town = town;
		this.county = county;
		this.country = country;
	}

	//Getters and setters
	public String getStreetLine() {
		return streetLine;
	}

	public void setStreetLine(String streetLine) {
		this.streetLine = streetLine;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString() {
		return	getStreetLine() + "\n" + 
				getTown() + "\n" + 
				getCounty() + "\n" + 
				getCountry() + "\n";
	}
	
	

	
	
	
	
	
	
	
	

}
