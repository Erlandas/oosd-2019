package lab3.Q1;

//Student Name 			: Erlandas Bacauskas
//Student Id Number		: C00242521
//Date 					: 29/9/2019
//Purpose 				: Working with calendar description in (lab 3 Exercises.txt)

import java.util.Calendar;

public class Clock {

	public static void main(String[] args) {

		//Instantiate calendar
		Calendar cal = Calendar.getInstance();
		
		//Instantiate time object
		//Pass as parameters current hour and minute
		Time t = new Time(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

		//Hold current minute
		int minute = t.getMinute() + 1;

		//Run until next minute
		while (t.getMinute() != minute) {
			
			//Store current ms
			long miliSec = System.currentTimeMillis();
			
			//Pause the outer loop while 1000ms (1s) passed
			while (System.currentTimeMillis() < miliSec + 1000) {

			}

			//Increment seconds
			t.tick();
			
			//Call to string to print time
			System.out.println(t.toString());

		}

	}
}
