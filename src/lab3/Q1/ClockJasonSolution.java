package lab3.Q1;

import java.util.Calendar;

public class ClockJasonSolution {

	public static void main(String[] args) {
		
		Calendar cal = Calendar.getInstance();
		
		Time t = new Time(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
		System.out.println(t.toString());
		
		long startTime = System.currentTimeMillis();
		long currentTime = startTime;
		int oldmin = cal.get(Calendar.MINUTE);
		int newmin = oldmin;
		
		while(newmin == oldmin) {
			
			//Wait for 1 second before calling the tick method
			while((currentTime - startTime) < 1000) {	
				currentTime = System.currentTimeMillis();
			}//end of inner while
			
			t.tick();
			System.out.println(t.toString());
			newmin = t.getMinute();
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			
		}//end outer while

	}

}
