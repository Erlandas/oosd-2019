package lab13;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Window extends JFrame{
	
	private JComboBox<String> jcb;
	private String[] colorsStrings = {"Red","Pink","Blue","Green"};
	private JCheckBox boxBackground = new JCheckBox("background");
	private JCheckBox boxForeground = new JCheckBox("foreground");
	
	public Window(String title) {
		super(title);
		getContentPane().setLayout(new BorderLayout());
		jcb = new JComboBox<String>(colorsStrings);
		add(jcb,BorderLayout.NORTH);
		JPanel panel = new JPanel();
		panel.add(boxBackground);
		panel.add(boxForeground);
		add(panel,BorderLayout.CENTER);
		
		JPanel panelSouth = new JPanel();
		JButton okBtn = new JButton("OK");
		JButton cancelBtn =new JButton("Cancel");
		panelSouth.add(okBtn);
		panelSouth.add(cancelBtn);
		add(panelSouth,BorderLayout.SOUTH);
		
		
	}
	

}
