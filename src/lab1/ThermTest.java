package lab1;

//Student Name 			: Erlandas Bacauskas
//Student Id Number		: C00242521
//Date 					: 20/9/2019
//Purpose 				: My first class implementation

public class ThermTest
{ // begin class ThermTest
	public static void main(String args[]) 
	{ // being main method

		Thermometer thermA = new Thermometer();		// Create an instance of our Thermometer class
		Thermometer thermB = new Thermometer(10.0);		// Create an instance of our Thermometer class
		
		double tempB = thermB.getCelsius();

		System.out.println("Temp. of Thermometer A is " + thermA.getCelsius() );
		thermA.setCelsius(20.0);
		System.out.println("Temp. of Thermometer A is " + thermA.getCelsius() );
		System.out.println("Temp. of Thermometer B is " + tempB );
		
		//Testing setFahrenheit() and getFahrenheit()
		tempB = thermB.getFahrenheit();
		System.out.println("Temperature in fahrenheit of thermometer B = " + tempB);
		
	} // end main
} // end class ThermTest
