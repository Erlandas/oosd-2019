package lab9.Q1;

import java.util.Scanner;

public class CD extends LibraryItem {

	private final double PRICE_PER_DAY = 0.09; 
	
	private String band;
	private String title;
	private int numTracks;

	public CD(String band, String title, int numTracks) {
		super("CD");
		this.band = band;
		this.title = title;
		this.numTracks = numTracks;
	}
	
	
	//Getters and Setters
	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNumTracks() {
		return numTracks;
	}

	public void setNumTracks(int numTracks) {
		this.numTracks = numTracks;
	}

	public double getPRICE_PER_DAY() {
		return PRICE_PER_DAY;
	}

	@Override
	public void calculatePrice() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("--<CD to take on loan>>--");
		System.out.println("Current price per day " + PRICE_PER_DAY + " euro cents");
		System.out.println("How many days? :");
		int days = scanner.nextInt();
		System.out.println("For " + days + " price is " + ((double) days * PRICE_PER_DAY));
	}

	@Override
	public String toString() {
		return super.toString()+ "\nBand : " +band+ "\nTitle : " +title+ "\nTracks : " +numTracks;
	}
	
	
	
}
