package lab9.Q1;

public class LibraryTest {

	public static void main(String[] args) {
		
		Book book = new Book("Jane Doe","Missing Person",123);
		System.out.println(book);
		
		CD cd = new CD("Queen","Killer Queen",12);
		System.out.println(cd);
		
		LibraryItem[] items = new LibraryItem[24];
		items[0] = book;
		items[1] = cd;
		
		for (LibraryItem libraryItem : items) {
			if(libraryItem != null) {
				libraryItem.calculatePrice();
			}
		}

	}

}
