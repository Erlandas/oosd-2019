package lab9.Q1;

public abstract class LibraryItem implements LoanItem {
	
	private static int idTracker = 0;

	private String type; 
	private String iD;
	
	public LibraryItem(String type) {
		this.type = type;
		this.iD = ""+idTracker;
		this.idTracker++;
	}
	
	//Getters and setters
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}
	
	@Override
	public abstract void calculatePrice();

	@Override
	public String toString() {
		
		return type + ", ID : " + iD;
	}
	
}
