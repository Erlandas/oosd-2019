package lab9.Q1;

import java.util.Scanner;

public class Book extends LibraryItem {
	
	private final double PRICE_PER_DAY = 0.13; 
	
	private String authorString;
	private String titleString;
	private int numPages;
	
	
	
	public Book(String authorString, String titleString, int numPages) {
		super("Book");
		this.authorString = authorString;
		this.titleString = titleString;
		this.numPages = numPages;
	}
	
	
	//Getters and setters
	public String getAuthorString() {
		return authorString;
	}

	public void setAuthorString(String authorString) {
		this.authorString = authorString;
	}

	public String getTitleString() {
		return titleString;
	}

	public void setTitleString(String titleString) {
		this.titleString = titleString;
	}

	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	public double getPRICE_PER_DAY() {
		return PRICE_PER_DAY;
	}



	@Override
	public void calculatePrice() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("--<BOOK to take on loan>>--");
		System.out.println("Current price per day " +PRICE_PER_DAY+ " euro cents");
		System.out.println("How many days? :");
		int days = scanner.nextInt();
		System.out.println("For " +days+ " price is " +((double)days*PRICE_PER_DAY));
	}

	@Override
	public String toString() { 
		
		return super.toString() + "\nAuthor : " +authorString+ "\nTitle : " +titleString+ "\nPages : " +numPages;
	}
	
	
	
	

}
