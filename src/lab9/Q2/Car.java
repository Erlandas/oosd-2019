package lab9.Q2;

public class Car extends RoadVehicle {
	private String carType;

	public Car() {
		this("", 0, 0);
	}

	public Car(String carType, int wheels, int passengers) {
		super(wheels, passengers);
		setType(carType);
	}

	public void setType(String t) {
		carType = t;
	}

	public String getType() {
		return carType;
	}
	
	@Override
	public double calculateDuty(double carPrice) {
		return carPrice * ImportDuty.CARTAXRATE;
	}
}
