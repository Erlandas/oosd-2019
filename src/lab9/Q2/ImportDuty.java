package lab9.Q2;

public interface ImportDuty {
	
	final double CARTAXRATE = 0.10d;
	final double HGVTAXRATE = 0.15d;
	
	double calculateDuty(double carPrice);
	
}
