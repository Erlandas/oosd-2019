package lab9.Q2;

public class Hgv extends RoadVehicle {
	private int cargo;

	public Hgv() {
		this(0, 0, 0);
	}

	public Hgv(int cargo, int wheels, int passengers) {
		super(wheels, passengers);
		setCargo(cargo);
	}

	public void setCargo(int cargo) {
		this.cargo = cargo;
	}

	public int getCargo() {
		return cargo;
	}
	
	@Override
	public double calculateDuty(double carPrice) {
		return carPrice*ImportDuty.HGVTAXRATE;
	}
}
