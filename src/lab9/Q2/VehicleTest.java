package lab9.Q2;

public class VehicleTest {

	public static void main(String[] args) {
		RoadVehicle car = new Car("Sedan", 5, 5);
		RoadVehicle hgv = new Hgv(2000, 6, 3);
		
		System.out.println("Car with price 2500 import duty is -> " + car.calculateDuty(2500.0d));
		System.out.println("HGV with price 5500 import duty is -> " + hgv.calculateDuty(5500.0d));
		
	}
}
