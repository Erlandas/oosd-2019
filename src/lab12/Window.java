package lab12;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Window extends JFrame {
	
	private JCheckBox chkSnapToGridBox;
	private JCheckBox chkShowGridBox;
	private JLabel lblX;
	private JLabel lblY;
	private JTextField txtX;
	private JTextField txtY;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnHelp;
	
	private JPanel pnlEast;
	private JPanel pnlCenter;
	private JPanel pnlWest;
	
	
	public Window(String title) {
		super(title);
		getContentPane().setLayout(new BorderLayout());
		initialiseComponents();
		
		pnlEast.add(btnOK);
		pnlEast.add(btnCancel);
		pnlEast.add(btnHelp);
		
		pnlCenter.add(lblX);
		pnlCenter.add(txtX);
		pnlCenter.add(lblY);
		pnlCenter.add(txtY);
		
		pnlWest.add(chkSnapToGridBox);
		pnlWest.add(chkShowGridBox);
		
		add(pnlWest, BorderLayout.WEST);
		add(pnlEast, BorderLayout.EAST);
		add(pnlCenter, BorderLayout.CENTER);


	}
	
	private void initialiseComponents() {
		chkSnapToGridBox = new JCheckBox("Snap to Grid");
		chkShowGridBox = new JCheckBox("Show Grid");
		lblX = new JLabel("X");
		lblY = new JLabel("Y:");
		txtX = new JTextField("8",5);
		txtY = new JTextField("8",3);
		btnOK = new JButton("OK");
		btnCancel = new JButton("Cancel");
		btnHelp = new JButton("Help");
		
		pnlEast = new JPanel(new GridLayout(3,1));
		pnlCenter = new JPanel(new GridLayout(2,2));
		pnlWest = new JPanel(new GridLayout(2,2));
	}

}
