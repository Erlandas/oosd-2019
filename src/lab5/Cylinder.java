package lab5;

public class Cylinder extends Circle {

	// Instance variables
	protected int height;

	// Constructors
	public Cylinder() {
		this(0, 0, 0, 0);
	}

	public Cylinder(int x, int y, int radius, int height) {
		super(x, y, radius);
		this.height = height;
	}

	// Getters and setters
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	// ToString
	@Override
	public String toString() {
		return "Cylinder [height=" + height + ", radius=" + radius + ", x=" + x + ", y=" + y + "]";
	}
}
