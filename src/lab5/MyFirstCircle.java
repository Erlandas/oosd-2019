package lab5;

//Author 		: Erlandas Bacauskas
//Date 			: 15/10/2019
//Purpose 		: A test driver program for our
//				: Point/Circle inheritance


public class MyFirstCircle {
	
	public static void main(String args[]) {
		Point myPoint = new Point(10, 20);

		Circle myCircle = new Circle(15, 30, 5);
		
		Cylinder myCylinder = new Cylinder(10,15,20,25);

		System.out.println("Point details : " + myPoint);
		System.out.println("Circle details: " + myCircle);
		System.out.println("Cylinder details: " + myCylinder);

	}

}
