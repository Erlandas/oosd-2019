package lab5;

public class Circle extends Point{
	
	//Instance variables
	protected int radius;
	
	//Default constructor
	public Circle() {
		this(0,0,0);
	}
	
	//Overloaded constructor with reference to super
	public Circle(int x, int y,int radius) {
		super(x,y);
		this.radius = radius;
	}

	//Getters and setters
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	//ToString
	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", x=" + x + ", y=" + y + "]";
	}
	
	

}
