package lab8.Q1;

public class Sphere extends ThreeDShape {

	private double radius;
	
	public Sphere(String name, String colour,double radius) {
		super(name, colour);
		this.radius = radius;
	}

	@Override
	public double area() {
		return 4*Math.PI*radius;
	}

	@Override
	public double volume() {
		return (4/3)*Math.PI*Math.pow(radius, 3);
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nRadius: " +radius;
	}

}
