package lab8.Q1;

public class Rectangle extends Shape {



	private double width;
	private double length;
	
	public Rectangle(String name, String colour, double length, double width) {
		super(name, colour);
		this.length=length;
		this.width=width;
	}
	@Override
	public double area() {
		// TODO Auto-generated method stub
		return length*width;
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nWidth: " +width+ "\nLength: " +length;
	}

}
