package lab8.Q1;

public class Cylinder extends ThreeDShape {
	
	private double height;
	private double radius;
	
	public Cylinder(String name, String colour, double radius, double height) {
		super(name, colour);
		this.radius = radius;
		this.height = height;
	}

	@Override
	public double area() {
		return (2*Math.PI*radius*height)+(2*Math.PI*radius*radius);
	}
	
	
	
	@Override
	public String toString() {
		return super.toString()+"\nHeight: " +height+ "\nRadius: " +radius;
	}

	@Override
	public double volume() {
		return Math.PI*Math.pow(radius, 2)*height;
	}
	

}
