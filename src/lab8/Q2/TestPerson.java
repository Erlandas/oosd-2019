package lab8.Q2;

public class TestPerson {

	public static void main(String[] args) {
		Person[] persons = new Person[2];
		Person john = new Employee("John", 9999.95);
		Person steven = new Student("Steven", "Gardening");
		
		persons[0] = john;
		persons[1] = steven;
		
		for (Person person : persons) {
			if(person != null) {
				System.out.println(person.getName() + " : " + person.getDescription());
			}
		}

	}

}
