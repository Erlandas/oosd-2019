package DSTutotial5;

public class Driver {
	
	public static void main(String[] args) {
		int[] array = {1,2,3,4,5,6,7,8};
		
		printArr(array);
		
		array = rotateArrMethodOne(array, 6);
//		System.out.println("\n----------------------------\n");
//		printArr(array);
//		
//		rotateArrMethodTwo1(array, 5);
		System.out.println("\n");
		printArr(array);
		
		
		
	}
	
	public static void rotateArrMethodTwo1(int[] a, int offset) {
		int size = a.length;
		offset = offset%size;
		
		int swapindexone = 0; 
		int swapIndexTwo = 0;
		
		for( int i = 0 ; i < a.length ; i++) {
			
			if(swapindexone == offset-1) {
				swapIndexTwo = ( ( offset + i ) % size );
			} else {
				swapindexone = ( ( offset + i ) % offset );
				swapIndexTwo = ( ( offset + i ) % size );
			}
			

				

			


			swapElements( a , swapindexone , swapIndexTwo );
			
			
			
			System.out.println("\nI = " +i+ " Swapping index " + swapindexone + " with index " +swapIndexTwo+ "\n");
			printArr(a);
		}
	}
	
	
	
	
	
	
	
	
	
	public static void rotateArrMethodTwo(int[] a, int offset) {
		offset = offset % a.length;
		
		int swapindexone = 0; 
		int swapIndexTwo = 0;
		int size = a.length;
		int end = a.length;
		
		for( int i = 0 ; i < end ; i++ ) {
			swapindexone = ( ( offset + i ) % offset );
			swapIndexTwo = ( ( offset + i ) % size );
			
			
				swapElements( a , swapindexone , swapIndexTwo );
				System.out.println("\nI = " +i+ " Swapping index " + swapindexone + " with index " +swapIndexTwo+ "\n");
				printArr(a);
			
		
			
		}
	}
	
	
//	//Method Two using swap method and One array
//	public static void rotateArrMethodTwo(int[] a, int offset) {
//		
//		if(offset == a.length) return;
//		else if (offset > a.length) offset = offset % a.length;
//		int end = offset;
//		end++;
//		end++;
//		end++;
//		end++;
////		if(offset > (a.length/2)) {
////			System.out.println("blabla bla" + a.length%2);
////			if(a.length%2 != 0) {
////				end++;
////				//end = ((a.length-offset) + 1);
////				System.out.println("if " + end);
////			} else {
////				end = a.length-offset + 1;
////				System.out.println("else");
////			}
////			
////		}
//
//		System.out.println("Offset -> " + offset);
//		System.out.println("End -> " + end);
//		for( int i = 0 ; i < end ; i++ ) {
//			swapElements( a , ( ( offset + i ) % offset ) , ( ( offset + i ) % a.length ) );
//			System.out.println("\nSwapping index " + ( ( offset + i ) % offset ) + " with index " + ( ( offset + i ) % a.length ) + "");
//			printArr(a);
//		}
//	}
	
	
	//Method One using temp array
	public static int[] rotateArrMethodOne(int[] a, int offset) {
		int[] tmp = new int[a.length];
		for( int i = 0 ; i < a.length ; i++ ) {
			tmp[(offset+i)%a.length] = a[i];
		}
		return tmp;
	}
	
	public static void swapElements(int[] a, int i, int j) {
		int tmp = a[j];
		a[j] = a[i];
		a[i] = tmp;
	}
	
	public static void printArr(int[] a) {
		
		for(int e: a) {
			System.out.print( e + ", " );
		}
	}
}
