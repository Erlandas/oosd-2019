package lab11;

//Converts a Fahrenheit temperature entered by the user to Celsius, or vice versa

import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ConvertTemp extends JFrame {
	
	private JTextField fahrenField = new JTextField();
	private JTextField celsiusField = new JTextField();
	
	// Constructor
	public ConvertTemp(String title)	{
		// Set title for frame and choose layout
		super(title);
		getContentPane().setLayout(new GridLayout(2, 2));

		// Add Fahrenheit label and text field to frame
		add(new JLabel("Fahrenheit"));
		
		//CONTROLS
		fahrenField.addActionListener(l -> {
			String fah = fahrenField.getText();
			double fahren = Double.parseDouble(fah);
			double cel = (fahren- 32.0d)*5.0/9.0;
			cel = Math.rint(cel * 100.0)/100.0;
			celsiusField.setText(""+cel);
		});
		
		celsiusField.addActionListener(l -> {
			String cel = celsiusField.getText();
			double cels = Double.parseDouble(cel);
			double fah = (cels*9.0/5.0)+32.0;
			fah = Math.rint(fah * 100.0)/100.0;
			fahrenField.setText(fah + "");
		});
		
		add(fahrenField);
		// Attach listener to text field

		// Add Celsius label and text field to frame
		add(new JLabel("Celsius"));
		add(celsiusField);
		// Attach listener to text field

		// Attach window listener
		addWindowListener(new WindowCloser());
	}//end costructor


	// Listener for window
	class WindowCloser extends WindowAdapter {
		public void windowClosing(WindowEvent evt) {
			System.exit(0);
		}
	}

}//end class