package lab10;
//Displays a frame containing two buttons. Pressing the
//"Lighter" button lightens the background of the frame.
//Pressing the "Darker" button darkens the background.

import java.awt.event.*;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ChangeColour extends JFrame{
	
	//Green blue red

		public ChangeColour(String title) {
		    // Set title, layout, and background colour
		    super(title);
		    setLayout(new FlowLayout());
		    getContentPane().setBackground(Color.gray);

		    //REQUIREMENTS FOR LAB
		    JButton btnLighter = new JButton("Lighter");
		    JButton btnDarker = new JButton("Darker");
		    JButton btnGreen = new JButton("Green");
		    JButton btnBlue = new JButton("Blue");
		    JButton btnRed = new JButton("Red");
		    
		    add(btnLighter);
		    add(btnDarker);
		    add(btnGreen);
		    add(btnBlue);
		    add(btnRed);
		    
		    btnLighter.addActionListener(l -> getContentPane().setBackground(Color.LIGHT_GRAY));
		    btnDarker.addActionListener(l -> getContentPane().setBackground(Color.DARK_GRAY));
		    btnGreen.addActionListener(l -> getContentPane().setBackground(Color.GREEN));
		    btnBlue.addActionListener(l -> getContentPane().setBackground(Color.blue));
		    btnRed.addActionListener(l -> getContentPane().setBackground(Color.RED));
		    
		    // Attach window listener
		    addWindowListener(new WindowCloser());
		}//end constructor

		// Listener for window
		class WindowCloser extends WindowAdapter {
			public void windowClosing(WindowEvent evt) {
		 		System.exit(0);
			}
		}

}//end class