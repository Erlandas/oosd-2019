package Swing.Login;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginForm extends JFrame implements ActionListener {
	
	  JPanel panel;
	  JLabel user_label, password_label, message;
	  JTextField userName_text;
	  JPasswordField password_text;
	  JButton login, register;

	public LoginForm() {
		
	      // Username Label
		user_label = new JLabel("Email Address : ");
//	      user_label = new JLabel();
//	      user_label.setText("User Name :");
	      userName_text = new JTextField(10);

	      // Password Label

	      password_label = new JLabel();
	      password_label.setText("Password :");
	      password_text = new JPasswordField(10);

	      // Submit

	      login = new JButton("Login");
	      register = new JButton("register");

	      panel = new JPanel(new GridBagLayout());
	      GridBagConstraints gc = new GridBagConstraints();
	      
	      gc.weightx= 1;
	      gc.weighty = 1;
	      
	      
	      //First line
	      gc.gridx = 0;
	      gc.gridy = 0;
	      gc.fill = GridBagConstraints.NONE;
	      
	      panel.add(user_label,gc);
	      
	      gc.gridx = 1;
	      gc.gridy = 0;
	      
	      panel.add(userName_text,gc);
	      
	      
	      //Second line
	      gc.gridx = 0;
	      gc.gridy = 1;
	      
	      panel.add(password_label,gc);
	      
	      gc.gridx = 1;
	      gc.gridy = 1;
	      
	      panel.add(password_text,gc);
	      
	      
	      //Third line
	      gc.gridx = 0;
	      gc.gridy = 2;

	      panel.add(register,gc);
	      
	      gc.gridx = 1;
	      gc.gridy = 2;
	      panel.add(login,gc);
	      
	      add(panel);
	      
	      login.addActionListener(al -> {
	    	  System.out.println("LOGIN");
	      });
	      
	      register.addActionListener(al -> {
	    	  System.out.println("REGISTER");
	      });
		
		
		//MAIN COMPONENTS FOR EACH JFRAME
		setLocation(400, 150);
		setTitle("Login Form");
		setSize(250,150);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
	}

	
}
