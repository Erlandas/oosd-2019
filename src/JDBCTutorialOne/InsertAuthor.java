package JDBCTutorialOne;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertAuthor {

	public static void main(String[] args) {
		
		final String DATABASE_URL = "jdbc:mysql://localhost/books";
		


		String firstName = "Tom";
		String secondName = "Walsh";
		
		try (Connection connection = DriverManager.getConnection(DATABASE_URL, "root", "");
			 PreparedStatement pstat = connection.prepareStatement("INSERT INTO Authors(firstname, lastname) VALUES (?,?)");) {

			pstat.setString(1, firstName);
			pstat.setString(2, secondName);
			
			pstat.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		
		
//		final String DATABASE_URL = "jdbc:mysql://localhost/books";
//		
//		Connection connection = null;
//		Statement statement = null;
//		String firstName = "John";
//		String secondName = "Wick";
//		
//		try {
//			connection = DriverManager.getConnection(DATABASE_URL, "root", "");
//			statement = connection.createStatement();
//			
//			PreparedStatement pstat = connection.prepareStatement("INSERT INTO Authors(firstname, lastname) VALUES (?,?)");
//			pstat.setString(1, firstName);
//			pstat.setString(2, secondName);
//			
//			pstat.executeUpdate();
//			
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} 
//		finally {
//			try {
//				statement.close();
//				connection.close();
//			} catch (Exception e2) {
//				e2.printStackTrace();
//			}
//		}

	}

}
