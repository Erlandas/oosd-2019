package JDBCTutorialOne;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteAuthor {

	public static void main(String[] args) {
		
		final String DATABASE_URL = "jdbc:mysql://localhost/books";
		
		int idToDelete = 8;
		String user = "root";
		String password = "";
		
		//Using prepared statements to delete
		
		try (Connection conn = DriverManager.getConnection(DATABASE_URL, user, password);
			PreparedStatement pstat = conn.prepareStatement("DELETE FROM authors WHERE authorID=?")) {
			
			pstat.setInt(1, idToDelete);
			pstat.executeUpdate();
			
		} catch (SQLException e) {
			System.out.println("Something went wrong : " + e.getMessage());
		}
		
//		try (Connection conn = DriverManager.getConnection(DATABASE_URL, user, password);
//			 Statement statement = conn.createStatement()) {
//			
//			statement.executeUpdate("DELETE FROM authors WHERE authorID='"+idToDelte+"' ");
//			
//		} catch (SQLException e) {
//			System.out.println("Something went wrong : " + e.getMessage());
//		}

	}

}
