package JDBCTutorialOne;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateAuthor {

	public static void main(String[] args) {
	
		
		final String DATABASE_URL = "jdbc:mysql://localhost/books";

		String firstName = "Bob";
		String secondName = "Updated";
		
		try (Connection connection = DriverManager.getConnection(DATABASE_URL, "root", "");
			Statement statement = connection.createStatement();	) {
			
			statement.executeUpdate("UPDATE authors SET lastname='"+secondName+"' WHERE firstname='"+firstName+"'");
			
		} catch (SQLException e) {
			System.out.println("Error -> " + e.getMessage());
		}	
		
//		final String DATABASE_URL = "jdbc:mysql://localhost/books";
//		
//		Connection connection = null;
//		Statement statement = null;
//		String firstName = "John";
//		String secondName = "Steven";
//		
//		try {
//			
//			connection = DriverManager.getConnection(DATABASE_URL, "root", "");
//			statement = connection.createStatement();
//
//			String commandString = "UPDATE authors SET lastname='"+secondName+"' WHERE firstname='"+firstName+"'";
//			statement.executeUpdate(commandString);
//			
//		} catch (SQLException e) {
//			System.out.println("Error -> " + e.getMessage());
//		}
//		finally {
//			try {
//				statement.close();
//				connection.close();
//			} catch (Exception e2) {
//				System.out.println("Error -> " + e2.getMessage());
//			}
//		}
	}

}
