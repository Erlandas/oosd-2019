package JDBCTutorialOne;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class DisplayAuthors {
	
	// database URL
	private static String DB_NAME = "books";
	private static String CONNECTION_STRING = "jdbc:mysql://localhost/" + DB_NAME;
	private static String user = "root";
	private static String password = "";
	
	private static final String COL_ID = "authorID";
	private static final String COL_FIRST_NAME = "firstname";
	private static final String COL_LAST_NAME = "lastname";
	private static final String TABLE_AUTHORS = "Authors";

	public static void main(String[] args) {
		
		//USING PREPARED STATEMENT
		
		try(Connection conn = DriverManager.getConnection(CONNECTION_STRING,user,password);
			PreparedStatement pstat = conn.prepareStatement("SELECT "+COL_ID+", "+COL_FIRST_NAME+", "+COL_LAST_NAME+" FROM "+TABLE_AUTHORS+"");
			ResultSet rs = pstat.executeQuery()) {
			
			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			System.out.println(TABLE_AUTHORS + " Table of Books Database:\n");
			
			for (int i = 1; i <= numberOfColumns; i++) {
				System.out.print(metaData.getColumnName(i) + "\t");
			}
			System.out.println();
			
			while (rs.next()) {
				for (int i = 1; i <= numberOfColumns; i++) {
					System.out.print(rs.getObject(i) + "\t\t");
				}
				System.out.println();
			}
			
		} catch (SQLException e) {
			System.out.println("Something went wrong : " + e.getMessage());
		}
				
		//TRY WITH RESOURCES 
//		try (Connection connection = DriverManager.getConnection(DATABASE_URL, "root", "");
//			 Statement statement = connection.createStatement();
//			 ResultSet resultSet = statement.executeQuery("SELECT authorID, firstname, lastname FROM Authors");) {
//	
//			// process query results
//			ResultSetMetaData metaData = resultSet.getMetaData();
//			int numberOfColumns = metaData.getColumnCount();
//			System.out.println("Authors Table of Books Database:\n");
//			
//			for (int i = 1; i <= numberOfColumns; i++) {
//				System.out.print(metaData.getColumnName(i) + "\t");
//			}
//			System.out.println();
//			
//			while(resultSet.next()) {
//				for (int i = 1; i <= numberOfColumns; i++) {
//					System.out.print( resultSet.getObject( i ) + "\t\t");
//				}
//				System.out.println();
//			}
//			
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		
//		Connection connection = null;
//		Statement statement = null;
//		ResultSet resultSet = null;
//		
//		try {
//			
//			// establish connection to database
//			connection = DriverManager.getConnection(DATABASE_URL, "root", "");
//			
//			// create Statement for querying database
//			statement = connection.createStatement();
//			
//			// query database
//			resultSet = statement.executeQuery("SELECT authorID, firstname, lastname FROM Authors");
//			
//			// process query results
//			ResultSetMetaData metaData = resultSet.getMetaData();
//			int numberOfColumns = metaData.getColumnCount();
//			System.out.println("Authors Table of Books Database:\n");
//			
//			for (int i = 1; i <= numberOfColumns; i++) {
//				System.out.print(metaData.getColumnName(i) + "\t");
//			}
//			System.out.println();
//			
//			while(resultSet.next()) {
//				for (int i = 1; i <= numberOfColumns; i++) {
//					System.out.print( resultSet .getObject( i ) + "\t\t");
//				}
//				System.out.println();
//			}
//			
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		finally {
//			try {
//				resultSet.close();
//				statement.close();
//				connection.close();
//			} catch(Exception ex) {
//				ex.printStackTrace();
//			}
//		}

	}

}
