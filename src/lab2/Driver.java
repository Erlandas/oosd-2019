package lab2;

//Student Name 			: Erlandas Bacauskas
//Student Id Number		: C00242521
//Date 					: 29/9/2019
//Purpose 				: Hotel room lab exercise description in (lab 2 Exercise.txt)

public class Driver {

	public static void main(String[] args) {
		
		//Create instances of hotelRoom type object
		HotelRoom roomA = new HotelRoom(200, "Single", true, 100.00d);
		HotelRoom roomB = new HotelRoom(201, "Double", false, 80.00d);
		HotelRoom roomC = new HotelRoom(202, "Single", false, 90.00d);

		//Print room type
		System.out.println(roomA.getRoomType());
		System.out.println(roomC.getRoomType());

		//Evaluation that we can't occupy already occupied room 
		if (roomB.isOccupied()) {
			System.out.println("Sorry, room is occupied");
		} else {
			System.out.println("Room is occupied for you");
			roomB.setOccupied(true);
		}

	}

}
