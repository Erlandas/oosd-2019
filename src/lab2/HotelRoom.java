package lab2;

public class HotelRoom {
	
	//Instance attributes
	private int roomNumber;
	private String roomType;
	private boolean isOccupied;
	private double rate;

	//Default constructor with default values
	public HotelRoom() {
		this.roomNumber = -1;
		this.roomType = "not specified";
		this.isOccupied = false;
		this.rate = -1.0d;
	}

	//Overloaded constructor
	public HotelRoom(int roomNumber, String roomType, boolean isOccupied, double rate) {
		this.roomNumber = roomNumber;
		this.roomType = checkIfDoubleOrSingle(roomType);
		this.isOccupied = isOccupied;
		this.rate = rate;
	}

	//Getter and setters
	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = checkIfDoubleOrSingle(roomType);
	}

	public boolean isOccupied() {
		return isOccupied;
	}
	
	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	//If room is already taken we not let to set to occupied again
	//Used boolean as return type just some sort of evaluation
	public boolean setOccupied(boolean isOccupied) {
		if (isOccupied() && isOccupied) {
			return false;
		}
		this.isOccupied = isOccupied;
		return true;
	}

	// ERROR check for a room type
	private String checkIfDoubleOrSingle(String roomType) {
		return (roomType.equalsIgnoreCase("SINGLE") || roomType.equalsIgnoreCase("DOUBLE")) ? roomType
				: "not specified correctly";
	}
}
