package lab14;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Printer extends JFrame {

	public Printer() {
		super("Printer");
		getContentPane().setLayout(new BorderLayout());
		
		//SOUTH BUTTON PANEL
		JPanel btnPanel = new JPanel(new GridLayout(4,1));
		JButton okBtn = new JButton("OK");
		JButton cancelBtn = new JButton("Cancel");
		JButton setupBtn = new JButton("Setup...");
		JButton helpBtn = new JButton("Help");
		btnPanel.add(okBtn);
		btnPanel.add(cancelBtn);
		btnPanel.add(setupBtn);
		btnPanel.add(helpBtn);
		add(btnPanel,BorderLayout.EAST);
		
		//CENTER PANEL THAT HOLDS OTHER PANELS
		JPanel centerPanel = new JPanel(new BorderLayout());
		
//		//Centre North panel of main
		JPanel lblPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel lbl = new JLabel("Printer: MyPrinter");
		lbl.setForeground(Color.blue);
		lblPanel.add(lbl);
		centerPanel.add(lblPanel,BorderLayout.NORTH);
		
//Bottom pannel
		
		JPanel bottomPanel = new JPanel();
		JLabel lbl2 = new JLabel("Print Quality:    ");
		lbl2.setForeground(Color.blue);
		bottomPanel.add(lbl2);
		
		String[] quality = {"High","Medium","Low"};
		JComboBox<String> comboBox = new JComboBox<String>(quality);
		bottomPanel.add(comboBox);
		
		JCheckBox chkBox = new JCheckBox("   Print to File   ");
		bottomPanel.add(chkBox);
		
		centerPanel.add(bottomPanel,BorderLayout.SOUTH);
		 
		JPanel middlePanel = new JPanel(new GridLayout(1, 5));
		middlePanel.setBackground(Color.PINK);
		
		JTextArea txtArea1 = new JTextArea(10, 1);
		middlePanel.add(txtArea1);
		
			JPanel checkBoxPanel = new JPanel(new GridLayout(3,1));
			checkBoxPanel.add(new JCheckBox("Image"));
			checkBoxPanel.add(new JCheckBox("Text"));
			checkBoxPanel.add(new JCheckBox("Code"));
			middlePanel.add(checkBoxPanel);
		
		JTextArea txtArea2 = new JTextArea(10, 1);
		middlePanel.add(txtArea2);	
			
			ButtonGroup group = new ButtonGroup();
			JRadioButton btn1Button =new JRadioButton("Selection");
			JRadioButton btn2Button =new JRadioButton("All");
			JRadioButton btn3Button =new JRadioButton("Applet");
			group.add(btn1Button);
			group.add(btn2Button);
			group.add(btn3Button);
			
			JPanel radioPanel = new JPanel(new GridLayout(3,1));
			radioPanel.add(btn1Button);
			radioPanel.add(btn2Button);
			radioPanel.add(btn3Button);
			middlePanel.add(radioPanel);
			
		JTextArea txtArea3 = new JTextArea(10, 1);
		middlePanel.add(txtArea3);	
			
		centerPanel.add(middlePanel, BorderLayout.CENTER);
		
		add(centerPanel, BorderLayout.CENTER);
		//add(bottomPanel, BorderLayout.SOUTH);
	}

}
