package lab14;

import javax.swing.JFrame;

public class Driver {

	public static void main(String[] args) {
		Printer printer = new Printer();
		printer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		printer.setVisible(true);
		printer.setSize(600, 200);
		printer.setLocation(500,300);
		
	}
}
